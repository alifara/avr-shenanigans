Uses `avr-gcc` for compiling and `avrdude` for flashing.

You can change the MCU as well as the programmer in the `Makefile`.

## Building and Flashing
Just do a `make` in any subfolder.
Alternatively, `make build` compiles the project and `make flash` flashes the compiled hex file.

## Development
If you use `ccls` for auto-completion, make sure you set the correct header directories in root `.ccls` file.

You can find them by running:

`$ avr-gcc -mmcu=atmega32a -E -dM -xc /dev/null -v 2>&1 | sed -n '/#include <...> search starts here:/,/End of search list./p'
`.
