/* #define F_CPU (800000ul) */
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
  DDRC = 0x01; /* all output */

  while (1) {
    PORTC ^= 0x01;
    _delay_ms(2000);
  }

  return 0;
}
